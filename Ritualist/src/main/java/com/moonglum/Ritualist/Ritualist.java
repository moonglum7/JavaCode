package com.moonglum.Ritualist;

import java.io.*;
import java.util.*;
import java.util.List;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
/**
 * @author Mark Dutton
 * @version 1.0.0
 * A tool to extract the rituals known by a character from the Character Builder file.
 * These rituals are then cross-referenced with the main CBLoader file to obtain details.
 * The fully detailed known rituals are then written out to a text file.
 * 
 * ============================= Revision History =============================
 * Date		Version		Who				Details
 * 04/08/15 1.0.0		Mark Dutton		Initial release
 * 
 */
public class Ritualist {
	private JFrame frameRitualistGUI;
	private JPanel pnlRitualistGUI, pnlCombinedFile, pnlCharacterFile, pnlOutputFile, pnlButtons;
	private JMenuBar menubarRitualistGUI;
	private JMenu helpRitualistGUI;
	private JMenuItem aboutRitualistGUI;
	private JButton btnChooseCombinedFile, btnChooseCharacterFile, btnChooseOutputFile, btnCreateRitualFile;
	private JTextField 	txtPathCombinedFile, txtPathCharacterFile, txtPathOutputFile,
						txtInstructionCombinedFile, txtInstructionCharacterFile, txtInstructionOutputFile;
	private File characterFile, combinedFile, outputFile;
	private HashSet<String> knownRituals;
	private HashSet<Ritual> allRituals;
	
	/**
	 * Launches Ritualist GUI.
	 */
	public static void main(String[] args) {
		Ritualist ritualist = new Ritualist();
		ritualist.buidGUI();
	}

	/**
	 * Builds and displays Ritualist GUI.
	 */
	private void buidGUI() {
		frameRitualistGUI = new JFrame();
		menubarRitualistGUI = new JMenuBar();
		helpRitualistGUI = new JMenu("Help");
		aboutRitualistGUI = new JMenuItem("About");		
		menubarRitualistGUI.add(helpRitualistGUI);
		helpRitualistGUI.add(aboutRitualistGUI);	
		frameRitualistGUI.setJMenuBar(menubarRitualistGUI);		
		pnlRitualistGUI = new JPanel();		
		pnlRitualistGUI.setLayout(new GridLayout(4, 1));		
		pnlCombinedFile  = new JPanel(new FlowLayout());
		pnlCharacterFile = new JPanel(new FlowLayout());
		pnlOutputFile = new JPanel(new FlowLayout());
		pnlButtons = new JPanel(new FlowLayout());
		txtInstructionCombinedFile = new JTextField("Choose Combined File", 12);
		txtInstructionCombinedFile.setEditable(false);
		txtInstructionCombinedFile.setBorder(null);
		txtInstructionCharacterFile = new JTextField("Choose Character File", 12);
		txtInstructionCharacterFile.setEditable(false); 
		txtInstructionCharacterFile.setBorder(null); 
		txtInstructionOutputFile = new JTextField("Choose Output File", 12);	
		txtInstructionOutputFile.setEditable(false);
		txtInstructionOutputFile.setBorder(null);
		btnChooseCombinedFile = new JButton("Choose");
		btnChooseCharacterFile = new JButton("Choose");
		btnChooseOutputFile = new JButton("Choose");
		btnCreateRitualFile = new JButton("Create Ritual File");
		txtPathCombinedFile = new JTextField("",20);
		txtPathCombinedFile.setEditable(false);
		txtPathCharacterFile = new JTextField("",20);
		txtPathCharacterFile.setEditable(false);
		txtPathOutputFile = new JTextField("",20);
		txtPathOutputFile.setEditable(false);		
		pnlCombinedFile.add(txtInstructionCombinedFile);
		pnlCombinedFile.add(btnChooseCombinedFile);
		pnlCombinedFile.add(txtPathCombinedFile);				
		pnlCharacterFile.add(txtInstructionCharacterFile);
		pnlCharacterFile.add(btnChooseCharacterFile);		
		pnlCharacterFile.add(txtPathCharacterFile);    	
		pnlOutputFile.add(txtInstructionOutputFile);
		pnlOutputFile.add(btnChooseOutputFile);
		pnlOutputFile.add(txtPathOutputFile);		
		pnlButtons.add(btnCreateRitualFile);		
		pnlRitualistGUI.add(pnlCombinedFile);
		pnlRitualistGUI.add(pnlCharacterFile);
		pnlRitualistGUI.add(pnlOutputFile);
		pnlRitualistGUI.add(pnlButtons);		
		frameRitualistGUI.getContentPane().add(pnlRitualistGUI);
		frameRitualistGUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameRitualistGUI.setSize(500,200);
		frameRitualistGUI.setVisible(true);
		frameRitualistGUI.setResizable(false);
		frameRitualistGUI.setTitle("Ritualist");		
		btnChooseCombinedFile.addActionListener(new CombinedFileListener());
		btnChooseCharacterFile.addActionListener(new CharacterFileListener());
		btnChooseOutputFile.addActionListener(new OutputFileListener());
		btnCreateRitualFile.addActionListener(new CreateRitualFileListener());
		aboutRitualistGUI.addActionListener(new AboutListener());
	}

	/**
	 * Extracts known rituals from specified Character Builder file. 
	 */
	private HashSet<String> extractKnownRituals(File file) {	
		HashSet<String> knownRituals = new HashSet<String>();
		try {
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file);
			XPath xPath =  XPathFactory.newInstance().newXPath();		
			NodeList knownRitualNodeList = (NodeList) xPath.compile("//RulesElement[@type='Ritual']").evaluate(doc, XPathConstants.NODESET);			
			for (int i = 0; i < knownRitualNodeList.getLength(); i++) {
				if (knownRitualNodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
					Element knownRitualElement = (Element) knownRitualNodeList.item(i);					
					knownRituals.add(knownRitualElement.getAttribute("internal-id"));
				}
			}
		} 
		catch (ParserConfigurationException e) { e.printStackTrace(); } 
		catch (SAXException e) { e.printStackTrace(); } 
		catch (IOException e) { e.printStackTrace(); } 
		catch (XPathExpressionException e) { e.printStackTrace(); }
		return knownRituals;
	}

	/**
	 * Extracts known rituals from specified main CBLoader file or serialised ritual extract file.
	 * If up-to-date serialised extract file is not found, rituals will be extracted from main CBLoader file
	 * via the extractRitualsFromUpdatedCombinedFile method. This data will be used to populate serialised extract 
	 * file for improved performance on future executions.
	 * If up-to-date serialised extract file is found it will be used for this execution.
	 */
	@SuppressWarnings("unchecked")
	private HashSet<Ritual> extractAllRituals() {
		File updatedRitualExtract = new File("Ritualist.ser");
		HashSet<Ritual> allRituals = new HashSet<Ritual>();
		try {
			if (updatedRitualExtract.exists() && (updatedRitualExtract.lastModified() > combinedFile.lastModified())) {
				ObjectInputStream is = new ObjectInputStream(new FileInputStream(updatedRitualExtract));
				allRituals =  (HashSet<Ritual>) is.readObject();
				is.close();
			} else {
				allRituals = extractRitualsFromUpdatedCombinedFile(combinedFile);
				ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(updatedRitualExtract));
				os.writeObject(allRituals);	
				os.close();
			}
		} 
		catch (FileNotFoundException e) { e.printStackTrace(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); } 
		catch (IOException e) { e.printStackTrace(); }		
		return allRituals;
	}
	
	/** 
	 * Extracts known rituals data from specified main CBLoader file, to populate a serialised extract file.
	 * This file will be used if possible, due to superior performance.
	 */
	private HashSet<Ritual> extractRitualsFromUpdatedCombinedFile(File file) {	
		HashSet<Ritual> updatedAllRituals = new HashSet<Ritual>();
		try {
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file);
			XPath xPath =  XPathFactory.newInstance().newXPath();					
			NodeList allRitualNodeList = (NodeList) xPath.compile("//RulesElement[@type='Ritual']").evaluate(doc, XPathConstants.NODESET);
			for (int i = 0; i < allRitualNodeList.getLength(); i++) {
				if (allRitualNodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
					Element allRitualElement = (Element) allRitualNodeList.item(i);			
					String internalID = allRitualElement.getAttribute("internal-id"); 					
					String name = allRitualElement.getAttribute("name");
					String flavorText, category, keySkill, componentCost, duration, level, time, 
							marketPrice, prerequisite, description;
					flavorText =  category =  keySkill = componentCost = duration = level = time =  
							marketPrice =  prerequisite = description = " ";
					NodeList allRitualDetailsNodeList = allRitualNodeList.item(i).getChildNodes();
					for (int j = 0; j < allRitualDetailsNodeList.getLength(); j++) {
						if (allRitualDetailsNodeList.item(j).getNodeType() == Node.TEXT_NODE) {
							description = allRitualDetailsNodeList.item(j).getTextContent();
						} 
						if (allRitualDetailsNodeList.item(j).getNodeType() == Node.ELEMENT_NODE) {							 
							Element allRitualDetailsElement = (Element) allRitualDetailsNodeList.item(j);
							if (allRitualDetailsElement.getNodeName() == "Flavor") {
								flavorText = allRitualDetailsElement.getTextContent();
							}
							if (allRitualDetailsElement.getNodeName() == "specific") {							
								switch (allRitualDetailsElement.getAttribute("name")) {															
								case "Category" 		: category = allRitualDetailsNodeList.item(j).getTextContent(); break;
								case "Key Skill" 		: keySkill = allRitualDetailsNodeList.item(j).getTextContent(); break;
								case "Component Cost" 	: componentCost = allRitualDetailsNodeList.item(j).getTextContent(); break;
								case "Duration" 		: duration = allRitualDetailsNodeList.item(j).getTextContent(); break;
								case "Level" 			: level = allRitualDetailsNodeList.item(j).getTextContent(); break;
								case "Time" 			: time = allRitualDetailsNodeList.item(j).getTextContent(); break;
								case "Market Price" 	: marketPrice = allRitualDetailsNodeList.item(j).getTextContent(); break;
								case "Prerequisite" 	: prerequisite = allRitualDetailsNodeList.item(j).getTextContent(); break;
								}
							}							
						}
					}
					updatedAllRituals.add(new Ritual(internalID, name, flavorText, category, keySkill, componentCost, duration, level, 
							time, marketPrice, prerequisite, description));					
				}
			}
		} 
		catch (ParserConfigurationException e) { e.printStackTrace(); } 
		catch (SAXException e) { e.printStackTrace(); } 
		catch (IOException e) { e.printStackTrace(); } 
		catch (XPathExpressionException e) { e.printStackTrace(); }
		return updatedAllRituals;
	}
		
	/** 
	 * Generates output file from selected input files.
	 */
	private void outputKnownRitualsToTextFile() {
		List<Ritual> allRitualsSorted = new ArrayList<Ritual>();
		allRitualsSorted.addAll(allRituals);
		Collections.sort(allRitualsSorted);
		if (outputFile != null && outputFile.exists()) { 
			outputFile.delete();
		}
		for (Ritual allRitualSorted : allRitualsSorted) {
			for (String knownRitual : knownRituals) {
				if (allRitualSorted.internalID.equals(knownRitual.toString())) {					
					PrintWriter writer = null;
					try {
						writer = new PrintWriter(new FileOutputStream(outputFile, true));						
						writer.write(allRitualSorted.toString());
						writer.write("\r\n\r\n");
					}
					catch (FileNotFoundException e) { e.printStackTrace(); }
					finally { writer.close(); }
				}
			}
		}		
	}	
	
	/** 
	 * Listener for selecting main CBLoader file.
	 */
	class CombinedFileListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser inputFile = new JFileChooser(System.getenv("APPDATA") + "\\CBLoader"); 
			inputFile.setFileFilter(new FileNameExtensionFilter("DND40 Files", "dnd40"));
			inputFile.showOpenDialog(frameRitualistGUI);
			combinedFile = inputFile.getSelectedFile();
			if (combinedFile != null) {
				txtPathCombinedFile.setText(combinedFile.getAbsolutePath());
			}
			else {
				txtPathCombinedFile.setText("");
				JOptionPane.showMessageDialog(frameRitualistGUI, "Please select a .dnd40 file","Ritualist",JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	/** 
	 * Listener for selecting Character Builder file.
	 */	
	class CharacterFileListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser inputFile = new JFileChooser();
			inputFile.setFileFilter(new FileNameExtensionFilter("DND40 Files", "dnd4e"));
			inputFile.showOpenDialog(frameRitualistGUI);
			characterFile = inputFile.getSelectedFile();				
			if (characterFile != null) {
				txtPathCharacterFile.setText(characterFile.getAbsolutePath());
			}
			else {
				txtPathCharacterFile.setText("");
				JOptionPane.showMessageDialog(frameRitualistGUI, "Please select a .dnd4e file","Ritualist",JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	/** 
	 * Listener for selecting output file.
	 */
	class OutputFileListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser saveFile = new JFileChooser();
			saveFile.showSaveDialog(frameRitualistGUI);
			outputFile = saveFile.getSelectedFile();
			if (outputFile != null) {
				txtPathOutputFile.setText(outputFile.getAbsolutePath());
			}
			else {
				txtPathOutputFile.setText("");
				JOptionPane.showMessageDialog(frameRitualistGUI, "Please select an output file","Ritualist",JOptionPane.WARNING_MESSAGE);
			}

		}
	}

	/** 
	 * Listener for generating output file.
	 */
	class CreateRitualFileListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if ((txtPathCombinedFile.getText() != null && !txtPathCombinedFile.getText().isEmpty()) && 
				(txtPathCharacterFile.getText() != null && !txtPathCharacterFile.getText().isEmpty()) &&
				(txtPathOutputFile.getText() != null && !txtPathOutputFile.getText().isEmpty())) {
				knownRituals = extractKnownRituals(characterFile);
				allRituals = extractAllRituals();
				outputKnownRitualsToTextFile();	
				JOptionPane.showMessageDialog(frameRitualistGUI, "Output file created","Ritualist",JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(frameRitualistGUI, "Please ensure all 3 files have been chosen","Ritualist",JOptionPane.WARNING_MESSAGE);
			}
		}
	}
	
	/** 
	 * Listener for displaying information about Ritualist.
	 */
	class AboutListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			JOptionPane.showMessageDialog(frameRitualistGUI, "Ritualist (by Moonglum)\r\nV1.0.1\r\nSource Coode available at https://github.com/Moonglum7/JavaCode/tree/master/Ritualist","About Rituaist",JOptionPane.INFORMATION_MESSAGE);
		}		
	}

}