package com.moonglum.Ritualist;

import java.io.Serializable;

/**
 * @author Mark Dutton
 * @version 1.0.0
 * Ritual object definition, allowing for a ritual to be handled as a discrete object.
 */
public class Ritual implements Serializable, Comparable<Ritual> {
	String internalID;
	String name;
	String flavorText;
	String category;
	String keySkill;
	String componentCost;
	String duration;
	String level;
	String time;
	String marketPrice;
	String prerequisite;
	String description;
	
	public Ritual(String internalID, String name, String flavorText, String category, String keySkill,
			String componentCost, String duration, String level, String time, String marketPrice,
			String prerequisite, String description ) {
		this.internalID = internalID;
		this.name = name;
		this.flavorText = flavorText;
		this.category = category;
		this.keySkill = keySkill;
		this.componentCost = componentCost;
		this.duration = duration;		
		this.level = level;
		this.time = time;
		this.marketPrice = marketPrice;
		this.prerequisite = prerequisite;
		this.description = description;
	}

	@Override
	public String toString() {		
		StringBuilder sbRitual = new StringBuilder(name);		 
		if (flavorText != null && !flavorText.trim().isEmpty()) {
			sbRitual.append("\r\n" + flavorText.trim()); 
		}
		sbRitual.append("\r\nLevel:" + level +
				"\r\nComponent Cost:" + componentCost + 
				"\r\nCategory:" + category +
				"\r\nMarket Price:" + marketPrice + 
				"\r\nTime:" + time + 
				"\r\nKey Skill:" + keySkill + 
				"\r\nDuration:" + duration);
		if (prerequisite != null && !prerequisite.trim().isEmpty()) {			
			sbRitual.append("\r\nPrerequisite:" + prerequisite);
		}
		sbRitual.append("\r\n" + description);
		return sbRitual.toString();
	}

	public int compareTo(Ritual r) {
		return name.compareTo(r.name);
	}

}